
export const environment = {
  production: true,
  hmr: false,
  apiHost: 'https://itpm-backend.cf/',
  apiPrefix: 'api/',
  adminPrefix: 'admin/',
  attachments: 'https://s3-us-west-2.amazonaws.com/elasticbeanstalk-us-west-2-959741697641/'
};
