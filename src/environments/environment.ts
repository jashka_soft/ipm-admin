
export const environment = {
  production: false,
  apiHost: 'http://localhost:8000/',
  apiPrefix: 'api/',
  adminPrefix: 'admin/',
  attachments: 'https://s3-us-west-2.amazonaws.com/elasticbeanstalk-us-west-2-959741697641/'
};
