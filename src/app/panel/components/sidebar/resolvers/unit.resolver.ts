import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';

import { Observable } from "rxjs/Observable";

import { IBreadcrumb, McBreadcrumbsResolver } from 'ngx-breadcrumbs';
import { AppHttpClient } from 'itpm-shared';

@Injectable()
export class UnitResolver extends McBreadcrumbsResolver {
  constructor(private AppHttpClient: AppHttpClient) {
    super();
  }

  public resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<IBreadcrumb[]> | Promise<IBreadcrumb[]> | IBreadcrumb[] {
    const id = route.params.id;

    return this.AppHttpClient.get(`units/${id}`);
  }
}