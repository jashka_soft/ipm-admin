import { Component } from '@angular/core';

import { IBreadcrumb, McBreadcrumbsService } from 'ngx-breadcrumbs';

@Component({
  selector: `sidebar`,
  templateUrl: `./sidebar.component.html`,
  styleUrls: [`./sidebar.component.css`]
})
export class SidebarComponent {
  public crumbs: IBreadcrumb[] = [];

  constructor(private McBreadcrumbsService: McBreadcrumbsService) {
  }

  public ngOnInit(): void {
    this.McBreadcrumbsService.crumbs$.subscribe(crumbs => {
      this.crumbs = crumbs;
    });
  }

  public ngOnDestroy(): void {
  }
}