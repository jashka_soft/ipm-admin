import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';

import { LoadingBarHttpClientModule } from '@ngx-loading-bar/http-client';
import { McBreadcrumbsModule } from 'ngx-breadcrumbs';

import { DashboardComponent } from './components/dashboard/dashboard.component';
import { NavigationComponent } from './components/navigation/navigation.component';
import { SidebarComponent } from './components/sidebar/sidebar.component';
import { SharedModule } from '../shared/shared.module';

import { ITPMModule } from 'itpm-shared';
import { environment } from '../../environments/environment';

const routes: Routes = [
  {
    path: '',
    component: DashboardComponent,
    data: {
      routeName: 'dashboard'
    },
    children: [
      {
        path: 'courses',
        loadChildren: './../course/course.module#CourseModule',
      },
      {
        path: 'definitions',
        loadChildren: './../definition/definition.module#DefinitionModule',
        data: {
          breadcrumbs: 'Поняття'
        }
      },
      {
        path: 'statistics',
        loadChildren: './statistics-wrap.module#StatisticsWrapModule'
      }
    ]
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes),

    McBreadcrumbsModule.forRoot(),
    LoadingBarHttpClientModule,

    SharedModule,
    ITPMModule.forRoot(environment),
  ],
  providers: [],
  declarations: [
    DashboardComponent,
    NavigationComponent,
    SidebarComponent,
  ]
})
export class PanelModule {
}
