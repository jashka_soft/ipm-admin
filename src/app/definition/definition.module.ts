import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { LoadingBarHttpClientModule } from '@ngx-loading-bar/http-client';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { DefinitionsComponent } from './components/definitons/definitions.component';
import { DefinitionService } from './components/definitons/definiton/definition.service';
import { DefinitionComponent } from './components/definitons/definiton/definition.component';
import { EditDefinitionComponent } from './components/definitons/definiton/definition-edit/edit-definition.component';
import { DefinitionFormComponent } from './components/definitons/definiton/definition-form/definition-form.component';

import { InterceptorMessageModule, ITPMModule, NamedRouteModule, PaginationModule } from 'itpm-shared';
import { environment } from '../../environments/environment';
import { DefinitionResolver } from '../panel/components/sidebar/resolvers/definition.resolver';

const routes: Routes = [
  {
    path: '',
    component: DefinitionsComponent,
    data: {
      routeName: 'definitions'
    }
  },
  {
    path: ':id/edit',
    component: EditDefinitionComponent,
    data: {
      routeName: 'editDefinition',
      breadcrumbs: 'Редагування {{ definition.name }}'
    },
    resolve: {
      definition: DefinitionResolver
    }
  },
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forChild(routes),

    LoadingBarHttpClientModule,
    NgbModule.forRoot(),

    NamedRouteModule,
    PaginationModule.forRoot(environment),
    ITPMModule.forRoot(environment),
    InterceptorMessageModule
  ],
  providers: [
    DefinitionService,

    DefinitionResolver,
  ],
  declarations: [
    DefinitionComponent,
    DefinitionsComponent,
    EditDefinitionComponent,
    DefinitionFormComponent
  ]
})
export class DefinitionModule {
}