import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Unit } from 'itpm-shared';
import { BaseItemFunctionality } from '../../../../shared/components/item/base-functionality';
import { ModalActiveUsersComponent } from './modal-active-users/modal-active-users.component';

@Component({
  selector: 'unit',
  templateUrl: `./unit.component.html`
})
export class UnitComponent extends BaseItemFunctionality implements OnInit {

  @Input() unit: Unit;

  public courseId: number;

  constructor(private ActivatedRoute: ActivatedRoute,
              private NgbModal: NgbModal) {
    super();

    this.courseId = +this.ActivatedRoute.snapshot.params['courseId'];
  }

  public ngOnInit() {
    this.routes.child.name = 'unitPage';
    this.routes.child.params = {
      'courseId': this.courseId,
      'lessonId': this.unit.lesson_id,
      'id': this.unit.id
    };

    this.routes.edit.name = 'editUnit';
    this.routes.edit.params = {
      'id': this.unit.id
    };
  }

  public removeBaseItem($event) {
    $event.preventDefault();
    $event.stopPropagation();

    this.unit.deleted_at = new Date();

    this.removeItem(this.unit.id);
  }

  public restoreBaseItem($event) {
    $event.preventDefault();
    $event.stopPropagation();

    this.unit.deleted_at = null;

    this.restoreItem(this.unit.id);
  }

  public showModalActiveUsers($event: Event) {
    $event.preventDefault();
    $event.stopPropagation();

    const ref = this.NgbModal.open(ModalActiveUsersComponent);
    ref.componentInstance.users = this.unit.users_active;
  }

  public cancelDefaultBehavior($event: Event) {
    $event.preventDefault();
    $event.stopPropagation();
  }

}