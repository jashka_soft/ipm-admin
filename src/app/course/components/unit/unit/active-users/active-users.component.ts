import { Component, Input } from '@angular/core';

import { User } from 'itpm-shared';

@Component({
  selector: 'examination-active-users',
  templateUrl: `./active-users.component.html`
})
export class ActiveUsersComponent {
  @Input() users: User[] = [];
}