import { Component, Input } from '@angular/core';

import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

import { User } from 'itpm-shared';

@Component({
  templateUrl: `./modal-active-users.component.html`
})
export class ModalActiveUsersComponent {
  @Input() users: User[] = [];

  public constructor(public activeModal: NgbActiveModal) {
  }
}