import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { Course } from 'itpm-shared';

@Component({
  selector: `course-form`,
  templateUrl: `./course-form.component.html`
})
export class CourseFormComponent implements OnInit {
  @Input() course: Course;
  @Input() btnActionText: string = 'Додати';
  @Input() btnBack: boolean = false;

  @Output() onSubmit = new EventEmitter<Course>();

  public courseGroup: FormGroup;

  constructor(private FormBuilder: FormBuilder) {
    this.courseGroup = FormBuilder.group({
      'name': [null, Validators.compose([Validators.required, Validators.minLength(5)])],
      'description': [null]
    });
  }

  ngOnInit(): void {
  }

  submit() {
    if (this.courseGroup.valid) {
      this.onSubmit.emit(this.course);
    }
  }

}