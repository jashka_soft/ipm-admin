import { Component, HostListener, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { Subscription } from 'rxjs/Subscription';
import 'rxjs/operator/map';

import { UnitService } from '../unit/unit/unit.service';

import { AuthService, DiagramSavedService, instances, Permissions, Schema, SchemaService, Unit } from 'itpm-shared';

@Component({
  selector: `schema-page`,
  templateUrl: `./schema-page.component.html`,
})
export class SchemaPageComponent implements OnInit, OnDestroy {
  public types: Array<Object> = [];
  public schema: Schema = null;
  public config: any = {};

  private isSaved: boolean = false;
  private subscription: Subscription;

  constructor(private DiagramSavedService: DiagramSavedService,
              private SchemaService: SchemaService,
              private UnitService: UnitService,
              private AuthService: AuthService,
              private ActivatedRoute: ActivatedRoute) {
    this.types = instances;
    this.config['role'] = this.AuthService.getUser().role;
    this.config['permission'] = Permissions.FULL_ACCESS;
    this.config['controls'] = true;
  }

  @HostListener('window:beforeunload', ['$event'])
  public onUnloadWindow($event) {
    return !this.isSaved ? $event.returnValue = true : null;
  }

  public ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  public ngOnInit() {
    this.subscription = this.UnitService.unit$.subscribe((unit: Unit) => {
      this.schema = unit.schema
        ? unit.schema
        : new Schema();
    });

    this.UnitService.callEvent('schema');
  }

  public onUpdateDiagram() {
    this.DiagramSavedService.new();
  }

  public save(schema: Schema) {
    schema.unit_id = +this.ActivatedRoute.snapshot.parent.params['id'];
    this.SchemaService.store(schema).subscribe((schema: Schema) => {
      this.UnitService.pushSchema(schema);
      this.isSaved = true;
      this.DiagramSavedService.save();
    });
  }

}