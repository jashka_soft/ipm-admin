import { Component } from '@angular/core';
import { Location as AngularBackPage } from '@angular/common';

@Component({
  selector: 'back-page',
  templateUrl: './back-page.component.html'
})
export class BackPageComponent {
  public constructor(private Location: AngularBackPage) {}

  public back() {
    this.Location.back();
  }
}