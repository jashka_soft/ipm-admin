import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { BaseItemComponent } from './components/item/base-item.component';
import { SearchItemComponent } from './components/search-item/search-item.component';

import { AuthModule, InterceptorMessageModule, ITPMModule, NamedRouteModule } from 'itpm-shared';
import { environment } from '../../environments/environment';
import { BackPageComponent } from './components/back-page/back-page.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    NgbModule.forRoot(),

    AuthModule,
    NamedRouteModule,
    ITPMModule.forRoot(environment),
    InterceptorMessageModule,
  ],
  providers: [],
  declarations: [
    BaseItemComponent,
    SearchItemComponent,
    BackPageComponent,
  ],
  exports: [
    BaseItemComponent,
    SearchItemComponent,
    BackPageComponent,
  ]
})
export class SharedModule {
}